/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;



import com.microsoft.aad.adal4j.AuthenticationContext;
import controller.Rest;
import controller.Utils;
import controller.WindowFollowListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;


/**
 * 
 *
 * @author Gabriel
 */
public class Principal extends javax.swing.JFrame {
    //clean compile assembly:single
    public String token;
    
    public Progresso barra = new Progresso(this, true);
    

       private void Filtrar(String texto, int coluna){
        TableRowSorter<DefaultTableModel> tr = new TableRowSorter<>(modelo);
        tabelaPaineis.setRowSorter(tr);
        if (coluna == -1){
            tr.setRowFilter(RowFilter.regexFilter("(?i)" + texto));
        }else{
            tr.setRowFilter(RowFilter.regexFilter("(?i)" + texto, coluna));
        }
    }   
    TableCellRenderer renderData = new DefaultTableCellRenderer() {

    SimpleDateFormat f = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        if( value instanceof Date) {
            value = f.format(value);
        }
        return super.getTableCellRendererComponent(table, value, isSelected,
                hasFocus, row, column);
    }
};
   
      TableCellRenderer renderLink = new DefaultTableCellRenderer() {

    SimpleDateFormat f = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

    public Component getTableCellRendererComponent(JTable table,
            Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        Color corOriginal = getForeground();
        if (column == 4 || column == 5){
            renderer.setForeground(Color.blue);
        } else
            renderer.setForeground(corOriginal);
        return super.getTableCellRendererComponent(table, value, isSelected,
                hasFocus, row, column);
    }
};
   
   
    static DefaultTableModel modelo = new DefaultTableModel(new Object [][] {},
    new String [] {
        "Id", "Cliente", "Painel", "Email", "Link do dataset", "Link público", "Últ. Atualização", "Status", "DataSource", "Agendamento"}
) {
    Class[] types = new Class [] {
        java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.util.Date.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
    };
    boolean[] canEdit = new boolean [] {
        false, false, false, false, false, false, false, false, false, false
    };
    public Class getColumnClass(int columnIndex) {
        return types [columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int Index) {
        return false;
    }
};

    
    
    /**
     * Creates new form Principal
     */
    public Principal() throws IOException, ParseException {
        controller.Painel.abreLeArquivo();
        
        int i;
        int x = controller.Painel.Paineis.size();    
        modelo.setRowCount(x);
        for(i=0; i < x; i++){
            modelo.setValueAt(controller.Painel.Paineis.get(i).getId(), i, 0);
            modelo.setValueAt(controller.Painel.Paineis.get(i).getCliente(), i, 1);
            modelo.setValueAt(controller.Painel.Paineis.get(i).getNomePainel(), i, 2);
            modelo.setValueAt(controller.Painel.Paineis.get(i).getEmail(), i, 3);
            modelo.setValueAt(controller.Painel.Paineis.get(i).getLinkPrivado(), i, 4);
            modelo.setValueAt(controller.Painel.Paineis.get(i).getLinkPublico(), i, 5);             
        }        
        this.setLocationByPlatform(true);
        initComponents();        
        new WindowFollowListener (barra, this);
        PopupMenuPrincipal menu = new PopupMenuPrincipal();        
        
        menu.opcaoAlterar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                    TelaCadastro telaAlterar = new TelaCadastro(view.Principal.this, true);
                    telaAlterar.setTitle("Alterar cadastro");
                    telaAlterar.setResizable(false);
            telaAlterar.setLocationRelativeTo(view.Principal.this);        
            try {    
            TelaCadastro.id = (int) modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 0);
            } catch(java.lang.ArrayIndexOutOfBoundsException evt){
                JOptionPane.showMessageDialog(view.Principal.this, "Nenhum item selecionado");
            }
                if (TelaCadastro.id > 0){
                    telaAlterar.tf_Cliente.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getCliente());
                    telaAlterar.tf_Email.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getEmail());
                    telaAlterar.tf_Painel.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getNomePainel());
                    telaAlterar.tf_LinkPub2.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getLinkPublico());
                    telaAlterar.tf_LinkPriv.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getLinkPrivado());
                    telaAlterar.isAtualizaPainel = true;
                    telaAlterar.show(true);
                    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                    telaAlterar.setLocation(dim.width/2-telaAlterar.getSize().width/2, dim.height/2-telaAlterar.getSize().height/2);        

                }
            }
        });
        
        menu.opcaoAtualizar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            Thread tredBarra = new Thread(new Runnable(){
                public void run(){
                    barra.dispose();
                    barra.setSize(500, 15);
                    barra.setLocationRelativeTo(view.Principal.this);
                    barra.setUndecorated(true);
                    barra.setVisible(true);

                }});
            tredBarra.start();
            Thread tred = new Thread(new Runnable(){
                public void run(){    
                    int x = tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow());
                    String[] retorno = null;
                    barra.barraProgresso.setMaximum(1);
                    barra.barraProgresso.setValue(0);
                    ExecutorService servico = controller.Auth.abreServico();
                    AuthenticationContext contexto = controller.Auth.contexto(servico);                    
                        try {        
                            String tokenUser = controller.Auth.pegaToken(modelo.getValueAt(x, 3).toString(), contexto)[0];
                            retorno = Rest.retornaUpdate(modelo.getValueAt(x, 4).toString(), tokenUser);
                            if (retorno[0] != null){
                                String data = retorno[0].replace("T", " ");
                                data = data.split("\\.")[0];
                                view.Principal.modelo.setValueAt(controller.Utils.StringToDataRet(data), x, 6);  
                                view.Principal.modelo.setValueAt(retorno[1], x, 7);
                                view.Principal.modelo.setValueAt(Rest.retornaStatusDataSource(modelo.getValueAt(x, 4).toString(), tokenUser), x, 8);
                                view.Principal.modelo.setValueAt(Rest.retornaStatusScheduleRefresh(modelo.getValueAt(x, 4).toString(), tokenUser), x, 9);
                                

                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, controller.Utils.breakLongString("Erro: " + ex, 100));
                            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                        }
                  barra.barraProgresso.setValue(1);
                  servico.shutdown();
                  barra.dispose();
                }
            });
            tred.start();
                }
        });
        
        menu.opcaoAgendamento.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            Thread tredBarra = new Thread(new Runnable(){
                public void run(){
                    barra.dispose();
                    barra.setSize(500, 15);
                    barra.setLocationRelativeTo(view.Principal.this);
                    barra.setUndecorated(true);
                    barra.setVisible(true);

                }});
            tredBarra.start();
            Thread tred = new Thread(new Runnable(){
                public void run(){    
                    int x = tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow());
                    String[] retorno = null;
                    barra.barraProgresso.setMaximum(1);
                    barra.barraProgresso.setValue(0);
                    ExecutorService servico = controller.Auth.abreServico();
                    AuthenticationContext contexto = controller.Auth.contexto(servico);                    
                        try {        
                            String tokenUser = controller.Auth.pegaToken(modelo.getValueAt(x, 3).toString(), contexto)[0];
                            retorno = Rest.retornaUpdate(modelo.getValueAt(x, 4).toString(), tokenUser);
                            Rest.ativaStatusScheduleRefresh(modelo.getValueAt(x, 4).toString(), tokenUser);
                            view.Principal.modelo.setValueAt(Rest.retornaStatusScheduleRefresh(modelo.getValueAt(x, 4).toString(), tokenUser), x, 9);

                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, controller.Utils.breakLongString("Erro: " + ex, 100));
                            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                        }
                  barra.barraProgresso.setValue(1);
                  servico.shutdown();
                  barra.dispose();
                }
            });
            tred.start();
                }
        });
        
        menu.opcaoRefresh.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            Thread tredBarra = new Thread(new Runnable(){
                public void run(){
                    barra.dispose();
                    barra.setSize(500, 15);
                    barra.setLocationRelativeTo(view.Principal.this);
                    barra.setUndecorated(true);
                    barra.setVisible(true);

                }});
            tredBarra.start();
            Thread tred = new Thread(new Runnable(){
                public void run(){    
                    int x = tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow());
                    String[] retorno = null;
                    barra.barraProgresso.setMaximum(1);
                    barra.barraProgresso.setValue(0);
                    ExecutorService servico = controller.Auth.abreServico();
                    AuthenticationContext contexto = controller.Auth.contexto(servico);                    
                        try {        
                            String tokenUser = controller.Auth.pegaToken(modelo.getValueAt(x, 3).toString(), contexto)[0];
                            Rest.refreshDataset(modelo.getValueAt(x, 4).toString(), tokenUser);
                            retorno = Rest.retornaUpdate(modelo.getValueAt(x, 4).toString(), tokenUser);
                            if (retorno[0] != null){
                                String data = retorno[0].replace("T", " ");
                                data = data.split("\\.")[0];
                                view.Principal.modelo.setValueAt(controller.Utils.StringToDataRet(data), x, 6);  
                                view.Principal.modelo.setValueAt(retorno[1], x, 7);
                                view.Principal.modelo.setValueAt(Rest.retornaStatusDataSource(modelo.getValueAt(x, 4).toString(), tokenUser), x, 8);
                                view.Principal.modelo.setValueAt(Rest.retornaStatusScheduleRefresh(modelo.getValueAt(x, 4).toString(), tokenUser), x, 9);
                            }
                            
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, controller.Utils.breakLongString("Erro: " + ex, 100));
                            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                        }
                  barra.barraProgresso.setValue(1);
                  servico.shutdown();
                  barra.dispose();
                }
            });
            tred.start();
                }
        });

        menu.opcaoHistorico.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){

            TelaCadastro.id = (int) modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 0);

            if (TelaCadastro.id > 0){
                view.HistoricoAtualizacoes.emailSelecionado = modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 3).toString();
                view.HistoricoAtualizacoes.linkPrivSelecionado = modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 4).toString();
                HistoricoAtualizacoes telaHistorico = new HistoricoAtualizacoes(view.Principal.this, true);
                telaHistorico.setTitle("Histórico de Atualizações");
                telaHistorico.setResizable(false);
                telaHistorico.setLocationRelativeTo(view.Principal.this);        
                telaHistorico.show(true);
                }
            }
        });
        
        tabelaPaineis.setComponentPopupMenu(menu);
        
        
        tabelaPaineis.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                int row = tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow());
                int column = tabelaPaineis.getSelectedColumn();
                if (SwingUtilities.isLeftMouseButton(e)){
                    try {
                        if (column == 4 || column == 5){
                            URI uri = new URI(modelo.getValueAt(row, column).toString());                        
                            controller.Utils.abrirURL(uri);
                        }                    
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                if (SwingUtilities.isRightMouseButton(e)){
                        int linha = tabelaPaineis.rowAtPoint(e.getPoint());
                        tabelaPaineis.changeSelection(linha, 0, false, false);
                }
                }
                
            }
        });       
      
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        comboFiltro = new javax.swing.JComboBox<>();
        tfFiltro = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        LabelUpdate = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaPaineis = new javax.swing.JTable();
        botaoAtualizar = new javax.swing.JButton();
        botaoAdicionar = new javax.swing.JButton();
        botaoAlterar = new javax.swing.JButton();
        botaoExcluir = new javax.swing.JButton();
        botaoHistorico = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Paineis Power BI");
        setMaximumSize(new java.awt.Dimension(1920, 1080));
        setMinimumSize(new java.awt.Dimension(900, 400));
        setPreferredSize(new java.awt.Dimension(1280, 600));

        jLabel2.setText("Valor do filtro:");

        comboFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "Cliente", "Painel", "Email" }));
        comboFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboFiltroActionPerformed(evt);
            }
        });

        tfFiltro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfFiltroKeyReleased(evt);
            }
        });

        jLabel3.setText("Filtrar campo:");

        javax.swing.GroupLayout topPanelLayout = new javax.swing.GroupLayout(topPanel);
        topPanel.setLayout(topPanelLayout);
        topPanelLayout.setHorizontalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, topPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        topPanelLayout.setVerticalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, topPanelLayout.createSequentialGroup()
                .addGap(0, 15, Short.MAX_VALUE)
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(tfFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)))
        );

        LabelUpdate.setText(" ");

        tabelaPaineis.setModel(modelo);
        tabelaPaineis.getColumnModel().getColumn(0).setPreferredWidth(5);
        tabelaPaineis.getColumnModel().getColumn(1).setPreferredWidth(90);
        tabelaPaineis.getColumnModel().getColumn(2).setPreferredWidth(90);
        tabelaPaineis.getColumnModel().getColumn(3).setPreferredWidth(100);
        tabelaPaineis.getColumnModel().getColumn(4).setPreferredWidth(160);
        tabelaPaineis.getColumnModel().getColumn(5).setPreferredWidth(150);
        tabelaPaineis.getColumnModel().getColumn(6).setPreferredWidth(60);
        tabelaPaineis.getColumnModel().getColumn(7).setPreferredWidth(40);
        tabelaPaineis.getColumnModel().getColumn(8).setPreferredWidth(40);
        tabelaPaineis.getColumnModel().getColumn(9).setPreferredWidth(40);
        tabelaPaineis.setAutoCreateRowSorter(true);
        //tablelaPaineis.setTableRenderer(Date.class, meuRender())
        tabelaPaineis.getColumnModel().getColumn(4).setCellRenderer(renderLink);
        tabelaPaineis.getColumnModel().getColumn(5).setCellRenderer(renderLink);
        tabelaPaineis.getColumnModel().getColumn(6).setCellRenderer(renderData);
        tabelaPaineis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaPaineisMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaPaineis);

        botaoAtualizar.setText("Atualizar");
        botaoAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoAtualizarActionPerformed(evt);
            }
        });

        botaoAdicionar.setText("Incluir");
        botaoAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoAdicionarActionPerformed(evt);
            }
        });

        botaoAlterar.setText("Alterar");
        botaoAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoAlterarActionPerformed(evt);
            }
        });

        botaoExcluir.setText("Excluir");
        botaoExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoExcluirActionPerformed(evt);
            }
        });

        botaoHistorico.setText("Histórico");
        botaoHistorico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoHistoricoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(botaoAtualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botaoAdicionar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botaoAlterar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botaoExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botaoHistorico)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(LabelUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(topPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(topPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botaoAtualizar)
                        .addComponent(botaoAdicionar)
                        .addComponent(botaoAlterar)
                        .addComponent(botaoExcluir)
                        .addComponent(LabelUpdate)
                        .addComponent(botaoHistorico)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfFiltroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfFiltroKeyReleased
        // TODO add your handling code here:
        String texto = tfFiltro.getText();
        String campo = comboFiltro.getSelectedItem().toString();
        if ("Todos".equals(campo)){
            Filtrar(texto, -1);
        }
        if ("Cliente".equals(campo)){
            Filtrar(texto, 1);
        }
        if ("Painel".equals(campo)){
            Filtrar(texto, 2);
        }
        if ("Email".equals(campo)){
            Filtrar(texto, 3);
        }
    }//GEN-LAST:event_tfFiltroKeyReleased

    private void comboFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboFiltroActionPerformed

    private void botaoAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoAdicionarActionPerformed
        try {
            controller.Painel.getUltimoID();
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        TelaCadastro telaPainel = new TelaCadastro(this, true);
        telaPainel.setTitle("Cadastrar painel");
        telaPainel.setResizable(false);
        telaPainel.setLocationRelativeTo(this);        
        telaPainel.show(true);
        
        //Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        //telaPainel.setLocation(dim.width/2-telaPainel.getSize().width/2, dim.height/2-telaPainel.getSize().height/2);        
    }//GEN-LAST:event_botaoAdicionarActionPerformed

    
    
    private void botaoAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoAtualizarActionPerformed
        Thread tredBarra = new Thread(new Runnable(){
            public void run(){
                barra.dispose();
                barra.setSize(500, 15);
                barra.setLocationRelativeTo(view.Principal.this);
                barra.setUndecorated(true);
                barra.setVisible(true);
                
            }});
        tredBarra.start();
        Thread tred = new Thread(new Runnable(){
            public void run(){    
                int x = modelo.getRowCount();
                String[] retorno = null;
                barra.barraProgresso.setMaximum(x);
                barra.barraProgresso.setValue(0);
                ExecutorService servico = controller.Auth.abreServico();
                AuthenticationContext contexto = controller.Auth.contexto(servico);
                
                for(int i = 0; i < x; i++){
                    try {        
                        String tokenUser  = controller.Auth.pegaToken(modelo.getValueAt(i, 3).toString(), contexto)[0];
                        retorno = Rest.retornaUpdate(modelo.getValueAt(i, 4).toString(), tokenUser);
                        if (retorno[0] != null){
                            String data = retorno[0].replace("T", " ");
                            data = data.split("\\.")[0];
                            view.Principal.modelo.setValueAt(controller.Utils.StringToDataRet(data), i, 6);  
                            view.Principal.modelo.setValueAt(retorno[1], i, 7);
                            view.Principal.modelo.setValueAt(Rest.retornaStatusDataSource(modelo.getValueAt(i, 4).toString(), tokenUser), i, 8);
                            view.Principal.modelo.setValueAt(Rest.retornaStatusScheduleRefresh(modelo.getValueAt(i, 4).toString(), tokenUser), i, 9);
                            
                        }
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, controller.Utils.breakLongString("Erro: " + ex, 100));
                        Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
              barra.barraProgresso.setValue(i+1);
              }
                servico.shutdown();
                LabelUpdate.setText("Atualizado em: " + Utils.dataToString(new Date()));
                barra.dispose();}
        });
        tred.start();
    }//GEN-LAST:event_botaoAtualizarActionPerformed

    private void tabelaPaineisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaPaineisMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_tabelaPaineisMouseClicked

    private void botaoAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoAlterarActionPerformed
        TelaCadastro telaAlterar = new TelaCadastro(this, true);
        telaAlterar.setTitle("Alterar cadastro");
        telaAlterar.setResizable(false);
        telaAlterar.setLocationRelativeTo(this);        
        try {    
        TelaCadastro.id = (int) modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 0);
        } catch(java.lang.ArrayIndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(this, "Nenhum item selecionado");
        }
            if (TelaCadastro.id > 0){
                telaAlterar.tf_Cliente.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getCliente());
                telaAlterar.tf_Email.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getEmail());
                telaAlterar.tf_Painel.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getNomePainel());
                telaAlterar.tf_LinkPub2.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getLinkPublico());
                telaAlterar.tf_LinkPriv.setText(controller.Painel.Paineis.get(TelaCadastro.id-1).getLinkPrivado());
                telaAlterar.isAtualizaPainel = true;
                telaAlterar.show(true);
                Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                telaAlterar.setLocation(dim.width/2-telaAlterar.getSize().width/2, dim.height/2-telaAlterar.getSize().height/2);        
                
            }
            
    }//GEN-LAST:event_botaoAlterarActionPerformed

    private void botaoExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoExcluirActionPerformed
        if (tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()) != -1) {
            //int input = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja exlcuir o registro?");
            int input = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja exlcuir o registro?", "Aviso", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
            if (input == JOptionPane.OK_OPTION) {
                try {
                    controller.Painel.DeletaPainel(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()));
                } catch (IOException ex) {
                    Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
                modelo.removeRow(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()));
                
                
            }
        }        
    }//GEN-LAST:event_botaoExcluirActionPerformed

    private void botaoHistoricoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoHistoricoActionPerformed
        // TODO add your handling code here:
        try {    
            TelaCadastro.id = (int) modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 0);
        } catch(Exception e){
            JOptionPane.showMessageDialog(this, "Nenhum item selecionado");
        }
            if (TelaCadastro.id > 0){
                view.HistoricoAtualizacoes.emailSelecionado = modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 3).toString();
                view.HistoricoAtualizacoes.linkPrivSelecionado = modelo.getValueAt(tabelaPaineis.convertRowIndexToModel(tabelaPaineis.getSelectedRow()), 4).toString();
                HistoricoAtualizacoes telaHistorico = new HistoricoAtualizacoes(this, true);
                telaHistorico.setTitle("Histórico de Atualizações");
                telaHistorico.setResizable(false);
                telaHistorico.setLocationRelativeTo(this);        
                telaHistorico.show(true);
            }
    }//GEN-LAST:event_botaoHistoricoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws IOException, ParseException {
        controller.Utils.criarDiretorioETexto("Paineis");
        //model.Painel p = new model.Painel(1, "Linear", "linear@alfameta.com.br", "Financeiro", "teste", "teste", null, null);
        //controller.ControllerPainel.AdicionaPainel(p);  
        
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    new Principal().setVisible(true);
                } catch (IOException | ParseException | ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelUpdate;
    private javax.swing.JButton botaoAdicionar;
    public static javax.swing.JButton botaoAlterar;
    private javax.swing.JButton botaoAtualizar;
    private javax.swing.JButton botaoExcluir;
    private javax.swing.JButton botaoHistorico;
    private javax.swing.JComboBox<String> comboFiltro;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable tabelaPaineis;
    private javax.swing.JTextField tfFiltro;
    private javax.swing.JPanel topPanel;
    // End of variables declaration//GEN-END:variables
}
