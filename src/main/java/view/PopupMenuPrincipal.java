/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 *
 * @author Gabriel
 */
public class PopupMenuPrincipal extends JPopupMenu{
    public JMenuItem opcaoAtualizar = new JMenuItem("Atualizar");
    public JMenuItem opcaoAgendamento = new JMenuItem("Ativar Agendamento");
    public JMenuItem opcaoRefresh = new JMenuItem("Refresh Dataset");
    public JMenuItem opcaoHistorico = new JMenuItem("Histórico");
    public JMenuItem opcaoAlterar = new JMenuItem("Alterar");
    public JMenuItem opcaoExcluir = new JMenuItem("Excluir");
    
    public PopupMenuPrincipal(){
        super();
                
        this.add(opcaoAtualizar);
        this.add(opcaoAgendamento);
        this.add(opcaoRefresh);
        this.add(opcaoHistorico);
        this.add(opcaoAlterar);
        this.add(opcaoExcluir);
        
        
        this.opcaoExcluir.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try {
                    int row = view.Principal.tabelaPaineis.getSelectedRow();
                    controller.Painel.DeletaPainel(row);
                    view.Principal.modelo.removeRow(row);
                } catch (IOException | ParseException ex) {
                    Logger.getLogger(PopupMenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
}
