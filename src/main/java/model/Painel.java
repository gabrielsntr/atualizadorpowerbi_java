/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Usuario
 */
public class Painel {
    public int id;
    public String cliente;
    public String email;
    public String nomePainel;
    public String linkPrivado;
    public String linkPublico;
    public Date dataAtualizacao;
    public String statusAtualizacao;
    public String dataSourceStatus;
    
    public Painel(int id, String cliente, String email, String nomePainel, String linkPrivado, String linkPublico){
        this.id = id;
        this.cliente = cliente;
        this.email = email;
        this.nomePainel = nomePainel;
        this.linkPrivado = linkPrivado;
        this.linkPublico = linkPublico;
    
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the nomePainel
     */
    public String getNomePainel() {
        return nomePainel;
    }

    /**
     * @param nomePainel the nomePainel to set
     */
    public void setNomePainel(String nomePainel) {
        this.nomePainel = nomePainel;
    }

    /**
     * @return the linkPrivado
     */
    public String getLinkPrivado() {
        return linkPrivado;
    }

    /**
     * @param linkPrivado the linkPrivado to set
     */
    public void setLinkPrivado(String linkPrivado) {
        this.linkPrivado = linkPrivado;
    }

    /**
     * @return the linkPublico
     */
    public String getLinkPublico() {
        return linkPublico;
    }

    /**
     * @param linkPublico the linkPublico to set
     */
    public void setLinkPublico(String linkPublico) {
        this.linkPublico = linkPublico;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
}
