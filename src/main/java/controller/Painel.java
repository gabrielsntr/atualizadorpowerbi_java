/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Usuario
 */

public class Painel {
    public static int ultimoID = 0;    
    Date expiracao;
    public static ArrayList<model.Painel> Paineis = new ArrayList();  
    
    public static void getUltimoID() throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Paineis.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        try (BufferedReader bf = new BufferedReader(conversor)) {
            boolean continua = true;
            String linha;
            while(continua){
                linha = bf.readLine();
                String[] painelLido = null;
                if(linha == null)
                    continua = false;
                else{
                    painelLido = linha.split(";");                    
                    ultimoID = Integer.parseInt(painelLido[0]);
                }
            }
        }
    }    
    
    public static void AdicionaPainel(model.Painel painel) throws IOException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Paineis.txt");
        FileWriter escrita = new FileWriter(arquivo, true);
        controller.Painel.Paineis.add(painel);
        escrita.write(painel.getId() + ";" + 
                painel.getCliente() + ";" + 
                painel.getEmail() + ";" + 
                painel.getNomePainel() + ";" + 
                painel.getLinkPrivado() + ";" + 
                painel.getLinkPublico() + ";" + 
                System.lineSeparator());
        escrita.close();
    }

    public static void AtualizaPainel(int id, model.Painel painelNovo) throws IOException, ParseException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Paineis.txt");
        abreLeArquivo();
        FileWriter escrita = new FileWriter(arquivo);
        Paineis.set(id-1, painelNovo);
        int i=0;
        for (i=0; i < Paineis.size(); i++){
            escrita.write(Paineis.get(i).getId() + ";" + Paineis.get(i).getCliente()+ ";" + 
                    Paineis.get(i).getEmail()+ ";" + Paineis.get(i).getNomePainel() + ";" + 
                    Paineis.get(i).getLinkPrivado() + ";" + Paineis.get(i).getLinkPublico() + ";" + 
                    System.lineSeparator());
        }
        escrita.close();
    } 
       
    public static void DeletaPainel(int id) throws IOException, ParseException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Paineis.txt");
        abreLeArquivo();
        FileWriter escrita = new FileWriter(arquivo);
        Paineis.remove(id);
        int i=0;
        for (i=0; i < Paineis.size(); i++){
            escrita.write(Paineis.get(i).getId() + ";" + Paineis.get(i).getCliente()+ ";" + 
                    Paineis.get(i).getEmail()+ ";" + Paineis.get(i).getNomePainel() + ";" + 
                    Paineis.get(i).getLinkPrivado() + ";" + Paineis.get(i).getLinkPublico() + ";" + 
                    System.lineSeparator());
        }
        escrita.close();
    }     
    
    
        public static void abreLeArquivo() throws IOException, ParseException{
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Paineis.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        Paineis.removeAll(Paineis);
        try (BufferedReader bf = new BufferedReader(conversor)) {
            boolean continua = true;
            String linha;
            while(continua){
                linha = bf.readLine();
                String[] painelLido = null;
                if(linha == null)
                    continua = false;
                else{
                    painelLido = linha.split(";", -1);
                    
                    Paineis.add(new model.Painel(Integer.parseInt(painelLido[0]), 
                            painelLido[1], painelLido[2], painelLido[3], 
                            painelLido[4], painelLido[5]));
                }
            }
            
        }
	in.close();
    }
    
    

}
