/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;

/**
 *
 * @author Usuario
 */
public class Tokens {
    

    public static void EscreveToken(String email, String[] token) throws IOException, ParseException{
        Utils.criarDiretorioETexto("Token");
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Token.txt");
        FileWriter escrita = new FileWriter(arquivo, true);
        PrintWriter limpa = new PrintWriter(arquivo);
        limpa.close();
        escrita.write(email + ";" + token[0] + ";" + 
                token[1]);
        escrita.close();
    }
    
    public static String[] AbreLeArquivo() throws IOException, ParseException{
        Utils.criarDiretorioETexto("Token");
        File diretorio = new File("Dados");
        File arquivo = new File(diretorio, "Token.txt");
        FileInputStream in = new FileInputStream(arquivo);
        InputStreamReader conversor = new InputStreamReader(in);
        String[] result = null;
        try (BufferedReader bf = new BufferedReader(conversor)) {
            String linha;
            linha = bf.readLine();
            String[] tokenLido = null;
            if(linha != null){
                tokenLido = linha.split(";", -1);
                result = tokenLido;
            }
        }        
	in.close();
        return result;
    }
    
    /*public static String AtualizaToken() throws IOException, ParseException{
        String[] result = null;      
        String[] tokenLido = AbreLeArquivo();
        if (tokenLido == null || Utils.VerificaData(Utils.StringToData(tokenLido[1])) > -180000){
            try {
                result = Auth.pegaToken(tokenLido[1]);
                EscreveToken(result);
            } catch (MalformedURLException | InterruptedException | ExecutionException ex) {
                Logger.getLogger(Tokens.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(Tokens.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            result = tokenLido;
        }
        return result[0];        
    }*/
    
}
