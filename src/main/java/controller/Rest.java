/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author Gabriel
 */
public class Rest {
    
    public static String pegaUltimaAtualizacao(String url, String token) throws MalformedURLException, IOException{
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/refreshes?$top=1");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;
        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
        
    return response.toString();   
    }
    
    public static JSONObject geraJSON(JSONObject response) throws IOException, JSONException{
        //JSONObject myobj = null;
        JSONObject o = null;
        
        //myobj = new JSONObject(PegaUltimaAtualizacao(url, token));
        
        JSONArray jArray = response.getJSONArray("value");
        if (!jArray.isNull(0)){
            o = jArray.getJSONObject(0);
        }
        //System.out.println(o.getString("endTime"));
        
        return o;
    }
    
    public static String[] retornaUpdate(String url, String token) throws IOException, JSONException{
        JSONObject objeto = geraJSON(new JSONObject(pegaUltimaAtualizacao(url, token)));
        String[] resultado;
        if (objeto == null){
            resultado  = new String[] {null, null};
        } else if ("Unknown".equals(objeto.getString("status"))){
            resultado = new String[] {objeto.getString("startTime"), "In progress"};
        } else {
            resultado = new String[] {objeto.getString("endTime"), objeto.getString("status")};
        }
        return resultado;
        
    }
    
    //Status do Gateway
    public static String pegaDataSource(String url, String token) throws MalformedURLException, IOException{
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/datasources");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;
        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
    return response.toString();   
    }
    
    public static String[] retornaDataSource(String url, String token) throws IOException, JSONException{
        JSONObject objeto = geraJSON(new JSONObject(pegaDataSource(url, token)));
        String[] resultado;
        if (objeto == null){
            resultado  = new String[] {null, null};
        } else {
            resultado = new String[] {objeto.getString("datasourceId"), objeto.getString("gatewayId")};
        }
    return resultado;
    } 
    
        public static int pegaStatusDataSource(String url, String token) throws MalformedURLException, IOException, JSONException{
            String[] datasource = retornaDataSource(url, token);
            URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/gateways/" + datasource[1] + "/datasources/" + datasource[0] + "/status");
            HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
            conn.setRequestProperty("Authorization","Bearer "+ token);
            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod("GET");
            
            int resposta = conn.getResponseCode();
            

        return resposta;   
        }
        
    public static String retornaStatusDataSource(String url, String token) throws IOException, MalformedURLException, JSONException {
        int status = pegaStatusDataSource(url, token);
        String resultado;
        if (status == 200){
            resultado  = "Online";
        } else {
            resultado = "Offline";
        }
    return resultado;
    }         
    
    //Atualizacao Agendada
    public static String pegaStatusScheduleRefresh(String url, String token) throws MalformedURLException, IOException{
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/refreshSchedule");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;
        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
    return response.toString();   
    }    
    
    
    public static String retornaStatusScheduleRefresh(String url, String token) throws IOException, JSONException{
        JSONObject objeto = new JSONObject(pegaStatusScheduleRefresh(url, token));
        boolean resultado = false;
        String retorno;
        if (objeto == null){
            resultado  = false;
        } else {
            resultado = objeto.getBoolean("enabled");
        }
        if (resultado == true){
            retorno = "Enabled";
        } else {
            retorno = "Disabled";
        }
    return retorno;
    }     
    
    public static void ativaStatusScheduleRefresh(String url, String token) throws MalformedURLException, IOException, JSONException{
        JSONObject value = new JSONObject();
        JSONObject enabled = new JSONObject();
        enabled.put("enabled", true);
        value.put("value", enabled);
        controller.SupportPatch.allowMethods("PATCH");
        
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/refreshSchedule");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setDoOutput(true);
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("PATCH");
        
        OutputStream os = conn.getOutputStream();
        os.write(value.toString().getBytes("UTF-8"));
        os.flush();
        os.close();
        conn.getResponseCode();
    }        
    
    public static void refreshDataset(String url, String token) throws MalformedURLException, IOException, JSONException{
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/refreshes");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        
        JSONObject value = new JSONObject();
        value.put("notifyOption", "NoNotification");        
        
        OutputStream os = conn.getOutputStream();
        os.write(value.toString().getBytes("UTF-8"));
        os.flush();
        os.close();
        conn.getResponseCode();        
    }
    
    public static String pegaAtualizacoes(String url, String token) throws MalformedURLException, IOException{
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/refreshes");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;
        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
            
    return response.toString();
    }

    public static ArrayList<String[]> retornaUpdates(String url, String token) throws IOException, JSONException{
        
        JSONArray arr = new JSONObject(pegaAtualizacoes(url, token)).getJSONArray("value");
        ArrayList<String[]> resultado;
        resultado = new ArrayList<String[]>();
        if (arr == null){
            resultado.add(new String[] {null, null});
        } else {
            for (int i = 0; i < arr.length(); i++) {
                try{
                    JSONObject obj = arr.getJSONObject(i);
                    String[] item = new String[] {obj.get("startTime").toString(), 
                        obj.get("endTime").toString(), obj.getString("refreshType"),
                    obj.getString("status")};
                    resultado.add(item);                    
                } catch(Exception e){ }
            }
        }
        return resultado;
    }        
    
    public static String pegaHorarioAtualizacoes(String url, String token) throws MalformedURLException, IOException{
        URL url_get = new URL("https://api.powerbi.com/v1.0/myorg/datasets/" + url.split("/")[6] + "/refreshSchedule");
        HttpURLConnection conn = (HttpURLConnection) url_get.openConnection();
        conn.setRequestProperty("Authorization","Bearer "+ token);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("GET");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output;
        StringBuffer response = new StringBuffer();
        while ((output = in.readLine()) != null) {
            response.append(output);
        }

        in.close();
    return response.toString();
    }   
    
    public static ArrayList<String[]> retornaHorarioAtualizacoes(String url, String token) throws IOException, JSONException{
        
        JSONArray arrDias = new JSONObject(pegaHorarioAtualizacoes(url, token)).getJSONArray("days");
        JSONArray arrHoras = new JSONObject(pegaHorarioAtualizacoes(url, token)).getJSONArray("times");
        ArrayList<String[]> resultado;
        resultado = new ArrayList<String[]>();
        if (arrDias == null){
            resultado.add(new String[] {null, null});
        } else {
            for (int i = 0; i < arrDias.length(); i++) {
                try{
                    String dia = arrDias.getString(i);
                    String hora = "";
                    switch (dia) {
                        case "Sunday":
                            dia = "Domingo";
                            break;
                        case "Monday":
                            dia = "Segunda-feira";
                            break;                            
                        case "Tuesday":
                            dia = "Terça-feira";
                            break;
                        case "Wednesday":
                            dia = "Quarta-feira";
                            break;
                        case "Thursday":
                            dia = "Quinta-feira";
                            break;
                        case "Friday":
                            dia = "Sexta-feira";
                            break;
                        case "Saturday":
                            dia = "Sábado";
                            break;  
                        default:
                            dia = "";
                    }
                    
                    for (int j = 0; j < arrHoras.length(); j++){
                        if (hora == "") {
                            hora = arrHoras.get(j).toString();
                        } else {
                            hora = hora + " | " + arrHoras.get(j).toString();
                        }
                    }                    
                    String[] item = new String[] {dia, hora};
                    resultado.add(item);
                } catch(Exception e){ }
            }
        }
        
        return resultado;
    }          
    
} 
    
    
