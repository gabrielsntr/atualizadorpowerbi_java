/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author Usuario
 */
public class Utils {
    
    public static void criarDiretorioETexto(String filename) throws IOException{
        File diretorio = new File("Dados");
        diretorio.mkdir();
        File arquivo = new File(diretorio, String.format("%s.txt", filename));
        arquivo.createNewFile();
    }

    public static String dataToString(Date data){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dataFormatada = df.format(data);
        return dataFormatada;
    }
    public static Date StringToData(String data) throws ParseException{
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date dataFormatada = df.parse(data);
        return dataFormatada;
    }
    
    public static Date StringToDataRet(String data) throws ParseException{
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dataFormatada = df.parse(data);
        dataFormatada = DateUtils.addHours(dataFormatada, -4);
        return dataFormatada;
    }

    
    public static long VerificaData(Date data){
        long agora = System.currentTimeMillis();
        long diff;
        diff = (agora - data.getTime());
        System.out.println(diff);
    
        return diff;
    }
    
    public static void abrirURL(URI uri) {
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().browse(uri);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Não foi possível abrir o link");
            }
        } else { /* TODO: error handling */ }
 }
       
    public static String breakLongString( String input, int charLimit ) {
    String output = "", rest = input;
    int i = 0;

     // validate.
    if ( rest.length() < charLimit ) {
        output = rest;
    }
    else if (  !rest.equals("")  &&  (rest != null)  )  // safety precaution
    {
        do
        {    // search the next index of interest.
            i = rest.lastIndexOf(" ", charLimit) +1;
            if ( i == -1 )
                i = charLimit;
            if ( i > rest.length() )
                i = rest.length();

             // break!
            output += rest.substring(0,i) +"\n";
            rest = rest.substring(i);
        }
        while (  (rest.length() > charLimit)  );
        output += rest;
    }
    return output;
}
    
}
